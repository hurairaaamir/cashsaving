<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable=['title','description','image','category_id','address','phone'];

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
