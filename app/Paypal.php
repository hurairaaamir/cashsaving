<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paypal extends Model
{
    protected $fillable=['trans_id','status','payer_email','payer_id','payer_address','payer_firstname','payer_surname','amount','currency_code'];
}
