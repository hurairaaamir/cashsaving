<?php

namespace App\Listeners;

use App\Events\MailEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\PaymentMail;
use App\Mail\ContactMail;
use App\Mail\FormMail;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

use Mail;

class MailListner
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MailEvent  $event
     * @return void
     */
    public function handle(MailEvent $event)
    {
        
        // // 
        // Mail::to("bankcoincredit@gmail.com")->send($email);



        // Instantiation and passing `true` enables exceptions
     $mail = new PHPMailer(true);

     try {




        //Server settings
        $mail->SMTPDebug = 0;                      // Enable verbose debug output
        // $mail->SMTPDebug = 2;                      // Enable verbose debug output

         $mail->isSMTP();                                            // Send using SMTP
         $mail->Host       = 'cp-wc05.iad01.ds.network';                    // Set the SMTP server to send through
         $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
         $mail->Username   = 'support@cashsavings4consumers.com';                     // SMTP username
         $mail->Password   = 'SUPPORT_007';                               // SMTP password
         $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
         $mail->SMTPSecure='ssl';
         $mail->Port       = 465;                                    // TCP port to connect to
 
         //Recipients
         $mail->setFrom('support@cashsavings4consumers.com', 'CashSavings4Consumers.com');
         $mail->addAddress('bankcoincredit@gmail.com');     // Add a recipient
        //  $mail->addAddress('your-recipient@gmail.com');               // Name is optional
        
        

        $mail->isHTML(true);

       
        if($event->type=="form")
        {
            
            $form=$event->data;

            $mail->Subject = 'Contact';

            $mail->Body    = '
            <h1>Payment Mail</h1>
            <div style="font-size:16px;">
            

            <b>Name:</b>'.$form->name.'<br>
            <b>Email:</b>'.$form->email.'<br>
            <b>Phone:</b>'.$form->phone.'<br>
            <b>Address:</b> '.$form->address.'<br>
            <b>Payment:</b>'.$form->payment.' AUS<br>
            <b>Code:</b>'.$form->code.'<br>
            </div>
            ';

        }else if($event->type=="contact")
        {
            $contact=$event->data;

            $mail->Subject = 'Contact';

            $mail->Body    = '
            <h1>Contact Mail</h1>
            <div style="font-size:16px;">
            <b>Name:</b>'.$contact->name.'<br>
            <b>Subject:</b> '.$contact->subject.'<br>
            <b>Email:</b> '.$contact->email.'<br>
            <b>Description:</b>'.$contact->description.'<br>
            <b>Phone no:</b> '.$contact->phone.'<br>
            <b>Code:</b> '.$contact->code.'<br>
            </div>
            ';

        }else if($event->type=="payment")
        {
            $paypal=$event->data;

            $mail->Subject = 'Paypal';

            $mail->Body    = '
            
            <h1>Paypal Mail</h1>
            <div style="font-size:16px;">
            <b>Transition id:</b>'.$paypal->trans_id.'<br>
            <b>Status:</b> '.$paypal->status.'<br>
            <b>Payer id:</b>'.$paypal->payer_id.'<br>
            <b>Payer First Name:</b>'.$paypal->payer_firstname.'<br>
            <b>Payer Surname Name :</b>'.$paypal->payer_surname.'<br>
            <b>Payer Email:</b>'.$paypal->payer_email.'<br>
            <b>Amount:</b> '.$paypal->amount.'<br>
            <b>Payer Address:</b>'.$paypal->payer_address.'<br>
            <b>Currency Code:</b>'.$paypal->currency_code.'<br>
            </div>
            ';
        }



         $mail->send();
     } catch (Exception $e) {
         print_r($e);
     }
    }
}


