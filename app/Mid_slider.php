<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mid_slider extends Model
{
    protected $fillable=['image'];
}
