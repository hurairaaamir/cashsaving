<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bcc extends Model
{
    protected $fillable=['image','bcc'];
}
