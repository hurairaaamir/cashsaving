<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Top_slider extends Model
{
    protected $fillable=['image'];
}
