<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Last_slider extends Model
{
    protected $fillable=['image'];
}
