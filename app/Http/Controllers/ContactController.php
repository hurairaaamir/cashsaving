<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Events\MailEvent;


class ContactController extends Controller
{
    public function index(){
        $contacts=Contact::all();
        return view('admin.contact',[
            "contacts"=>$contacts
        ]);
    }
    public function store(Request $request){
        $request->validate([
            "description"=>"required",
            "email"=>"email"
        ]);
        $contact=Contact::create([
            "name"=>$request->name,
            "description"=>$request->description,
            "email"=>$request->email,
            "subject"=>$request->subject,
            "phone"=>$request->phone,
            "code"=>$request->code
        ]);

        $type='contact';
        event(new MailEvent($type,$contact));

        return response()->json([
            "data"=>"Message successfully sent thank you for contacting us!"
        ]);
    }
    public function destroy($id){
        $contact=Contact::findOrFail($id);
        $contact->delete();

        return redirect()->back()->with('success','Message Deleted Successfully');
    }
}
