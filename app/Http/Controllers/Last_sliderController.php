<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Last_slider;

class Last_sliderController extends Controller
{
    public function index()
    {
        $last_sliders=Last_slider::all();
        return view('admin.last_slider',[
            'last_sliders'=>$last_sliders
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'image'=>'required'
        ]);

        $image = $request->image;
        $image_name = uniqid(). '.' . $image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/last_slider/';
        $image->move($destinationPath, $image_name);
        $path1='/images/last_slider/'.$image_name;

        Last_slider::create([
            "image"=>$path1
        ]);

        return redirect()->back()->with('success','Slide Created Successfully');
    }

    public function destroy($id){
        $last_slider=Last_slider::findOrFail($id);

        $path=public_path($last_slider->image);            
        
        @unlink($path);

        $last_slider->delete();

        return redirect()->back()->with('success','Slide Deleted Successfully');
    }
}
