<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\MassDestroyProductRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Product;

class ProductsController extends Controller
{
    public function index()
    {
        abort_unless(\Gate::allows('product_access'), 403);

        $products = Product::all();

        return view('admin.products.index', compact('products'));
    }

    public function create()
    {
        abort_unless(\Gate::allows('product_create'), 403);

        return view('admin.products.create');
    }

    public function store(Request $request)
    {
        abort_unless(\Gate::allows('product_create'), 403);


        $request->validate([
            "name"=>"required",
            "description"=>"required",
            "price"=>"required",
            "image"=>'required|image|mimes:jpg,png,jpeg|max:4000'
        ]);

        $image = $request->image;
        $image_name = uniqid(). '.' . $image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/products/';
        $image->move($destinationPath, $image_name);
        // dd($request->category->id);
        Product::create([
            "name"=>$request->name,
            "description"=>$request->description,
            "price"=>$request->price,
            "image"=>$image_name,
        ]);




        // $product = Product::create($request->all());

        return redirect()->route('admin.products.index');
    }

    public function edit(Product $product)
    {
        abort_unless(\Gate::allows('product_edit'), 403);

        return view('admin.products.edit', compact('product'));
    }

    public function update($id,Request $request)
    {
        abort_unless(\Gate::allows('product_edit'), 403);
        
        $request->validate([
            "name"=>"required",
            "description"=>"required",
            // "image"=>'required|image|mimes:jpg,png,jpeg|max:4000',
            "price"=>'required'
        ]);

        $product=Product::findOrFail($id);

        if($request->image){
            
            $path=public_path()."/images/products/".$product->image;

            @unlink($path);
        
            $image = $request->image;
            $image_name = uniqid().'.'.$image->getClientOriginalExtension();
            $destinationPath=public_path().'/images/products/';
            $image->move($destinationPath, $image_name);

            $product->update([
                "name"=>$request->name,
                "description"=>$request->description,
                "price"=>$request->price,
                "image"=>$image_name,
            ]);

        }else{
            $product->update([
                "name"=>$request->name,
                "description"=>$request->description,
                "price"=>$request->price,
            ]);
        }



        $product->update($request->all());

        return redirect()->route('admin.products.index');
    }

    public function show(Product $product)
    {
        abort_unless(\Gate::allows('product_show'), 403);

        return view('admin.products.show', compact('product'));
    }

    public function destroy(Product $product)
    {
        abort_unless(\Gate::allows('product_delete'), 403);

        $path=public_path()."/images/products/".$product->image;
        
        @unlink($path);

        $product->delete();

        return back();
    }

    public function massDestroy(MassDestroyProductRequest $request)
    {
        Product::whereIn('id', request('ids'))->delete();

        return response(null, 204);
    }
}
