<?php

namespace App\Http\Controllers\Admin;
use App\Bcc;

class HomeController
{
    public function index()
    {
        $bccs=Bcc::all();
        return view('home',[
            "bccs"=>$bccs
        ]);
    }
}
