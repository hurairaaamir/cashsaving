<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $categories=Category::all();
        return view('admin.items.categories.index',[
            'categories'=>$categories
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required'
        ]);

        Category::create([
            "name"=>$request->name
        ]);

        return redirect('/admin/categories')->with('success','Category Created Successfully');
    }

    public function update($id,Request $request){
        $category=Category::findOrFail($id);
        
        $category->update([
            "name"=>$request->name
        ]);

        return redirect('/admin/categories')->with('success','Category Updated Successfully');
    }
    public function destroy($id){
        $category=Category::findOrFail($id);

        foreach($category->cards as $card){
            $path='/images/Cards/'.$card->image;
            @unlink($path);
            
            $card->delete();
        }

        $category->delete();

        return redirect('/admin/categories')->with('success','category deleted Successfully');
    }
    
}
