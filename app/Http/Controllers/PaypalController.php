<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paypal;
use App\Events\MailEvent;

class PaypalController extends Controller
{
    public function index(){
        $paypals=Paypal::all();

        return view('admin.paypal',[
            "paypals"=>$paypals
        ]);
    }

    public function store(Request $request){
        $data=$request->details;

        $paypal=Paypal::create([
            "trans_id"=>$data['id'],
            "status"=>$data['status'],
            "payer_id"=>$data['payer']['payer_id'],
            "payer_email"=>$data['payer']['email_address'],
            "payer_address"=>$data["payer"]['address']['country_code'],
            "payer_firstname"=>$data['payer']['name']['given_name'],
            "payer_surname"=>$data['payer']['name']['surname'],
            "amount"=>$data['purchase_units'][0]['amount']['value'],
            "currency_code"=>$data['purchase_units'][0]['amount']['currency_code']
        ]);
        $type='payment';
        event(new MailEvent($type,$paypal));

        return redirect()->back()->with('success','Transaction Made Successfully');
    }
    public function destroy($id){
        $paypal=Paypal::findOrFail($id);
        $paypal->delete();  

        return redirect()->back()->with('success','Paypal Deleted Successfully');
    }
    public function sendMail(){
        $paypal="paypal";
        event(new MailEvent($paypal));
    }
}
