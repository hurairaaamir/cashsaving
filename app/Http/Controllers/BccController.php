<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bcc;

class BccController extends Controller
{
    public function index(){
        $bccs=Bcc::all();
        return view('admin.bcc',[
            "bccs"=>$bccs
        ]);
    }
    public function store(Request $request)
    {
        // dd($request);

        $request->validate([
            "bcc"=>"required",
            "image"=>'required|image|mimes:jpg,png,jpeg|max:4000',
        ]);

        $image = $request->image;
        $image_name = uniqid(). '.' . $image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/bcc/';
        $image->move($destinationPath, $image_name);
        $path1='/images/bcc/'.$image_name;
        
        // dd($request->category->id);
        $bcc=Bcc::create([
            "bcc"=>$request->bcc,
            "image"=>$path1,
        ]);


        return redirect('/admin/bcc')->with('success','BCC Created successfully');

    }

    public function update($id,Request $request)
    {
        $request->validate([
            "bcc"=>"required",
        ]);

        $bcc=Bcc::findOrFail($id);

        if($request->image){
            $path=public_path($bcc->image);            

            @unlink($path);

            // dd($delPath);
            // @unlink($delPath);

            $image = $request->image;
            $image_name = uniqid().'.'.$image->getClientOriginalExtension();
            $destinationPath=public_path().'/images/bcc/';
            $image->move($destinationPath, $image_name);
            $path1='/images/bcc/'.$image_name;
            

            $bcc->update([
                "bcc"=>$request->bcc,
                "image"=>$path1,
            ]);
        }else{
            $bcc->update([
                "bcc"=>$request->bcc,
            ]);
        }

        return redirect('/admin/bcc')->with('success','BCC Updated Successfully');
    }

    public function destroy($id){
        
        $bcc=Bcc::findOrFail($id);

        $path=public_path($bcc->image);            

        @unlink($path);

        $bcc->delete();

        return redirect('/admin/bcc')->with('success','BCC Deleted Successfully');
        
    }
}
