<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\MailEvent;
use App\Payment;

class PaymentController extends Controller
{
    public function show($cost){
        return view('payment',[
            "cost"=>$cost
        ]);
    }
    
    public function index(){
        $payments=Payment::all();

        return view('admin.payment',[
            "payments"=>$payments
        ]);
    }
    public function store(Request $request){
        $request->validate([
            "name"=>"required|min:3|max:255",
            "email"=>"required|email",
            "phone"=>"required|min:9|max:14",
            "payment"=>"required",
            "code"=>"required"
        ]);
        $payment=Payment::create([
            "name"=>$request->name,
            "email"=>$request->email,
            "phone"=>$request->phone,
            "address"=>$request->address,
            "payment"=>$request->payment,
            "code"=>$request->code
        ]);
        $type='form';
        event(new MailEvent($type,$payment));
        
        $payment=$request->payment;
        return view('paypal',[
            "payment"=>$payment
        ]);
    }
    public function destroy($id){
        $payment=Payment::findOrFail($id);
        $payment->delete();

        return redirect()->back()->with("success",'Payment Deleted Successfully');
    }

    public function sendthis(){
        $payment='eat this!';
        $type='form';
        event(new MailEvent($type,$payment));
    }
    
}
