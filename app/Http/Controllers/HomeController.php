<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bcc;
use App\Top_slider;
use App\Mid_slider;
use App\Last_slider;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index()
    {
        $bccs=Bcc::all();

        $top_sliders=Top_slider::all();

        $mid_sliders=Mid_slider::all();

        $last_sliders=Last_slider::all();

        return view('home',[
            "bccs"=>$bccs,
            "top_sliders"=>$top_sliders,
            "mid_sliders"=>$mid_sliders,
            "last_sliders"=>$last_sliders
        ]);
    }
}
