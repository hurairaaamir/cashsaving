<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Card;
use App\Category;

class CardController extends Controller
{
    public function index()
    {
        $cards=Card::all();

        return view('admin.items.cards.index',[
            'cards'=>$cards
        ]);
    }

    public function create()
    {
        $categories=Category::all();
        return view('admin.items.cards.create',[
            "categories"=>$categories
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            "title"=>"required",
            "description"=>"required",
            "image"=>'required|image|mimes:jpg,png,jpeg|max:4000',
            "category"=>'required'
        ]);
        // dd($request);

        $image = $request->image;
        $image_name = uniqid(). '.' . $image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/Cards/';
        $image->move($destinationPath, $image_name);
        $path1='/images/Cards/'.$image_name;
        // dd($request->category->id);
        Card::create([
            "title"=>$request->title,
            "description"=>$request->description,
            "image"=>$path1,
            "phone"=>$request->phone,
            "address"=>$request->address,
            "category_id"=>$request->category
        ]);

        return redirect('/admin/cards')->with('success','Card Created successfully');

    }

    public function edit($id)
    {
        $categories=Category::all();
        $card=Card::findOrFail($id);

        return view('admin.items.cards.edit',[
            "categories"=>$categories,
            "card"=>$card

        ]);
    }

    public function update($id,Request $request)
    {
        $request->validate([
            "title"=>"required",
            "description"=>"required",
            // "image"=>'required|image|mimes:jpg,png,jpeg|max:4000',

            "category"=>'required'
        ]);

        $card=Card::findOrFail($id);

        if($request->image){
            // $delPath = asset('images/Cards/'.$card->image);
            $path=public_path($card->image);            
            @unlink($path);
            // dd($delPath);
            // @unlink($delPath);

            $image = $request->image;
            $image_name = uniqid().'.'.$image->getClientOriginalExtension();
            $destinationPath=public_path().'/images/Cards/';
            $image->move($destinationPath, $image_name);
            $path1='/images/Cards/'.$image_name;

            $card->update([
                "title"=>$request->title,
                "description"=>$request->description,
                "image"=>$path1,
                "phone"=>$request->phone,
                "address"=>$request->address,
                "category_id"=>$request->category
            ]);
        }else{
            $card->update([
                "title"=>$request->title,
                "description"=>$request->description,
                "phone"=>$request->phone,
                "address"=>$request->address,
                "category_id"=>$request->category
            ]);
        }

        return redirect('/admin/cards')->with('success','Card updated successfully');
    }

    public function destroy($id){
        $card=Card::findOrFail($id);

        $path=public_path($card->image);            

        @unlink($path);

        $card->delete();

        return redirect()->back()->with('success','Card Deleted Successfully');
    }


    public function getCardProducts(){
        $categories=Category::all();
        $cards=Card::where('category_id',$categories[0]->id)->get();
        // $cards=Card::all();

        // return response()->json([
        //     "cards"=>$cards,
        //     "categories"=>$categories,
        //     "key"=>$categories[0],
        // ]);
        return view('product',[
            "categories"=>$categories,
            "cards"=>$cards
        ]);
    }

    public function getByCategory($id){
        $cards=Card::where('category_id',$id)->get();
        $output="";
        if(!$cards->isEmpty()){
            foreach($cards as $card){
                $output.='
                <div class="card my-card">
                    <div class="card-header cd-h" >
                        <img src="'.$card->image.'" class="pro-img">
                    </div>
                    <div class="card-body" style="text-align:center;">
                        <div class="cd-title"><h4>'.$card->title.'</h4></div>
                        <div class="cd-comp"><p>'.$card->address.'</p></div>
                        <div class="cd-des" ><p>'.$card->description.'</p></div>
                        <div class="cd-ph" ><h6>'.$card->phone.'</h6></div>
                    </div>
                </div>
                ';
            }
        }
        // echo $output;
        return response()->json([
            "data"=>$output
        ]);
    }

}
