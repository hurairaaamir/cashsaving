<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mid_slider;

class Mid_sliderController extends Controller
{
    public function index()
    {
        $mid_sliders=Mid_slider::all();
        return view('admin.mid_slider',[
            'mid_sliders'=>$mid_sliders
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'image'=>'required'
        ]);

        $image = $request->image;
        $image_name = uniqid(). '.' . $image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/mid_slider/';
        $image->move($destinationPath, $image_name);
        $path1='/images/mid_slider/'.$image_name;

        Mid_slider::create([
            "image"=>$path1
        ]);

        return redirect()->back()->with('success','Slide Created Successfully');
    }

    public function destroy($id){
        $mid_slider=Mid_slider::findOrFail($id);

        $path=public_path($mid_slider->image);            

        @unlink($path);

        $mid_slider->delete();

        return redirect()->back()->with('success','Slide Deleted Successfully');
    }

}
