<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Top_slider;

class Top_sliderController extends Controller
{
    public function index()
    {
        $top_sliders=Top_slider::all();
        return view('admin.top_slider',[
            'top_sliders'=>$top_sliders
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'image'=>'required'
        ]);

        $image = $request->image;
        $image_name = uniqid(). '.' . $image->getClientOriginalExtension();
        $destinationPath=public_path(). '/images/top_slider/';
        $image->move($destinationPath, $image_name);
        $path1='/images/top_slider/'.$image_name;

        Top_slider::create([
            "image"=>$path1
        ]);

        return redirect()->back()->with('success','Category Created Successfully');
    }

    public function destroy($id){
        $top_slider=Top_slider::findOrFail($id);
        
        $path=public_path($top_slider->image);            

        @unlink($path);

        $top_slider->delete();

        return redirect()->back()->with('success','Category Deleted Successfully');
    }

}
