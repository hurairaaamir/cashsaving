<?php

Route::get('/', 'HomeController@index');

Route::get('/admin', function(){
    
    return view('admin');
})->middleware('auth');

Route::get('/home','HomeController@index');


Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

    Route::resource('users', 'UsersController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');

    Route::resource('products', 'ProductsController');
});



Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {



});
Route::get('admin/cards', 'CardController@index')->name('admin.cards');

Route::get('/admin/card/create','CardController@create');

Route::post('/admin/card/store','CardController@store');

Route::get('/admin/card/{id}/edit','CardController@edit');

Route::put('/admin/card/{id}/update','CardController@update');

Route::delete('/admin/card/{id}/delete','CardController@destroy');

Route::get('/admin/product','ProductController@index');


Route::get('admin/categories', 'CategoryController@index')->name('admin.categories');


Route::get('/admin/category/create','CategoryController@create');

Route::post('/admin/category/store','CategoryController@store');

Route::get('/admin/category/{id}/edit','CategoryController@edit');

Route::put('/admin/category/{id}/update','CategoryController@update');

Route::delete('/admin/category/{id}/delete','CategoryController@destroy');

Route::get('/product','CardController@getCardProducts');

// Route::get('/getCardProducts','CardController@getCardProducts');

Route::get('/getByCategory/{id}','CardController@getByCategory');




Route::get('/admin/bcc','BccController@index');

Route::get('/admin/bcc/create','BccController@create');

Route::post('/admin/bcc/store','BccController@store');

Route::get('/admin/bcc/{id}/edit','BccController@edit');

Route::put('/admin/bcc/{id}/update','BccController@update');

Route::delete('/admin/bcc/{id}/delete','BccController@destroy');






Route::get('/admin/top_slider','Top_sliderController@index');

Route::post('/admin/top_slider/store','Top_sliderController@store');

Route::put('/admin/top_slider/{id}/update','Top_sliderController@update');

Route::delete('/admin/top_slider/{id}/delete','Top_sliderController@destroy');




Route::get('/admin/mid_slider','Mid_sliderController@index');

Route::post('/admin/mid_slider/store','Mid_sliderController@store');

Route::put('/admin/mid_slider/{id}/update','Mid_sliderController@update');

Route::delete('/admin/mid_slider/{id}/delete','Mid_sliderController@destroy');



Route::get('/admin/last_slider','Last_sliderController@index');

Route::post('/admin/last_slider/store','Last_sliderController@store');

Route::put('/admin/last_slider/{id}/update','Last_sliderController@update');

Route::delete('/admin/last_slider/{id}/delete','Last_sliderController@destroy');





Route::get('/admin/paypal','PaypalController@index');

Route::post('/sendPaypal','PaypalController@store');

Route::delete('admin/paypal/{id}/delete','PaypalController@destroy');





Route::get('/sales',function(){
    return view('sales');    
});
Route::get('/payment/{cost}','PaymentController@show')->name('payment');

Route::get('/payment',function(){
    return view('payment',[
        "cost"=>NULL
    ]);    
});

Route::get('/admin/payment','PaymentController@index');

Route::post('/payment','PaymentController@store');

Route::delete('/admin/payment/{id}/delete','PaymentController@destroy');

Route::get('/sendMail','PaypalController@sendMail');



Route::get('/install', function(){
    echo exec("pwd");
    echo exec("cd ../ && php artisan key:generate");
    echo exec("cd ../ && php artisan migrate --seed");

    echo exec("php artisan key:generate");
    echo exec("php artisan migrate --seed");
});







Route::get('/admin/contact','ContactController@index');

Route::post('/contact','ContactController@store')->name('contact');

Route::delete('/admin/contact/{id}/delete','ContactController@destroy');

Route::get('/opportunity',function(){
    return view('opportunity');
});

Route::get('/sendthis','PaymentController@sendthis');

Route::get('/test','PdfController@test');