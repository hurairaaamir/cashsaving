<?php

Route::get('/fresh', function(){
    echo exec('cd ../ && git pull');

    echo exec('cd ../ && composer install');

    echo "<br><br>composer installed <br><br>";

    echo exec("cd ../ && cp .env.example .env");

    echo "<br><br> .env file created <br><br>";

    Artisan::call('key:generate');

    echo "<br><br>Key generated <br><br>";
});


Route::get('/database', function(){
    Artisan::call('migrate:fresh');

    echo "<br><br> migration done <br><br>";

    Artisan::call('db:seed');

    echo "<br><br> seeding done <br><br>";
});

Route::get('/arbitrary', function(){
    Artisan::call('queue:work');

    echo "<br><br> queue worker started <br><br>";
});

