$(document).ready(function () {
  let $btnss = $("#tabs .product-tabs li");
  console.log($btnss);
  $btnss.click(function (e) {
    $("#tabs .product-tabs li a").removeClass("active");
    e.target.classList.add("active");

    let selector = $(e.target).attr("data-filter");
    $(".products .grid-1").isotope({
      filter: selector,
    });

    return false;
  });
});

$(function () {
  $("#client-slider").owlCarousel({
    autoplay: 1000,
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
  });
});
$(function () {
  $("#saving-slider").owlCarousel({
    autoplay: 1000,
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
  });
});

$(function () {
  $("#bottom-slider").owlCarousel({
    autoplay: 1500,
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
  });
});

$(function () {
  $("#deadground-slider").owlCarousel({
    autoplay: 1000,
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
  });
});
