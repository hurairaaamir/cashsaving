const nav_slide = () => {
    let burger = document.querySelector('.burger');
    let linkSection = document.querySelector('.nav-links');
    let nav = document.querySelector('nav');
    let links = document.querySelectorAll('.nav-links li');

    burger.addEventListener('click', () => {
        
        if (nav.classList.contains('nav-active')) {
            nav.style.height = '205px';

            nav.classList.remove('nav-active');

            linkSection.style.transform = 'translateY(-100%)';
        } else {
            nav.style.height = '400px';
            nav.classList.add('nav-active');
            linkSection.style.transform = 'translateY(0%)';

        }
        links.forEach((link, index) => {
            if (link.style.animation) {
                link.style.animation = ''
            } else {
                link.style.animation = `navLinkFade 0.5s ease forwards ${index /7 +1}s`;
            }
        });

        burger.classList.toggle('toggle');

    });
}


nav_slide();

