<?php

use Illuminate\Database\Seeder;
use App\Card;
use App\Category;
use App\Bcc;
use App\Top_slider;
use App\Mid_slider;
use App\Last_slider;


class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
        ]);
        factory(Category::class,3)->create();
        factory(Card::class,10)->create();

        $bccs=[
            0=>[
                "image"=>'/images/bcc/glasses.jpg',
                "bcc"=>'100%'
            ],
            1=>[
                "image"=>'/images/bcc/gold.jpg',
                "bcc"=>'100%'
            ],
            2=>[
                "image"=>'/images/bcc/iphone.jpg',
                "bcc"=>'50%'
            ],
            3=>[
                "image"=>'/images/bcc/perfume.jpg',
                "bcc"=>'100%'
            ],
            4=>[
                "image"=>'/images/bcc/shoes.jpg',
                "bcc"=>'100%'
            ],
            5=>[
                "image"=>'/images/bcc/underlay.jpg',
                "bcc"=>'100%'
            ]
        ];
        foreach($bccs as $bcc){
            Bcc::create($bcc);
        }


        $top_sliders=[
            0=>[
                "image"=>'/images/top_slider/1.jpg',
            ],
            1=>[
                "image"=>'/images/top_slider/2.jpg',
            ],
            2=>[
                "image"=>'/images/top_slider/3.jpg',
            ],
            3=>[
                "image"=>'/images/top_slider/4.jpg',
            ],
            4=>[
                "image"=>'/images/top_slider/6.jpg',
            ],
            5=>[
                "image"=>'/images/top_slider/7.jpg',
            ],
        ];
        foreach($top_sliders as $slider){
            Top_slider::create($slider);
        }


        $mid_sliders=[
            0=>[
                "image"=>'/images/mid_slider/1.jpg',
            ],
            1=>[
                "image"=>'/images/mid_slider/4.jpg',
            ],
            2=>[
                "image"=>'/images/mid_slider/3.jpg',
            ],
            3=>[
                "image"=>'/images/mid_slider/2.webp',
            ],
            4=>[
                "image"=>'/images/mid_slider/5.jpg',
            ],
            5=>[
                "image"=>'/images/mid_slider/6.jpg',
            ],
            6=>[
                "image"=>'/images/mid_slider/7.jpg',
            ],
        ];
        foreach($mid_sliders as $slider){
            Mid_slider::create($slider);
        }



        $last_sliders=[
            0=>[
                "image"=>'/images/last_slider/1.jpg',
            ],
            1=>[
                "image"=>'/images/last_slider/7.jpg',
            ],
            2=>[
                "image"=>'/images/last_slider/3.jpg',
            ],
            3=>[
                "image"=>'/images/last_slider/4.jpg',
            ],
            4=>[
                "image"=>'/images/last_slider/5.jpg',
            ],
            5=>[
                "image"=>'/images/last_slider/6.jpg',
            ],
            6=>[
                "image"=>'/images/last_slider/2.jpg',
            ],
        ];
        foreach($last_sliders as $slider){
            Last_slider::create($slider);
        }

    }
}
