@extends('layouts.admin')

@section('styles')
<style>
.my-column{
    min-width:200px;
}

</style>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        Paypal
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="cardTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th class="my-column">
                            transaction id
                        </th>
                        <th class="my-column">
                            payer id
                        </th>
                        <th class="my-column">
                            payer first name
                        </th>
                        <th class="my-column">
                            payer surname
                        </th>
                        <th class="my-column">
                            payer email
                        </th>
                        <th class="my-column">
                            payer address
                        </th>
                        <th class="my-column">
                            amount
                        </th>
                        <th class="my-column">
                            currency code
                        </th>
                        <th style="min-width:100px;">
                            Time
                        </th>
                        <th class="my-column">
                            status
                        </th>
                        <th class="my-column">
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($paypals as $paypal)

                        <tr>
                            <td class="my-column">
                                {{$paypal->trans_id}}
                            </td>
                            <td class="my-column">
                                {{$paypal->payer_id}}
                            </td>
                            <td class="my-column">
                                {{$paypal->payer_firstname}}
                            </td>
                            <td class="my-column">
                                {{$paypal->payer_surname}}
                            </td>
                            <td class="my-column">
                                {{$paypal->payer_email}}
                            </td>
                            <td class="my-column">
                                {{$paypal->payer_address}}
                            </td>
                            <td class="my-column">
                                {{$paypal->amount}}
                            </td>
                            <td class="my-column">
                                {{$paypal->currency_code}}
                            </td>
                            <td class="my-column">
                                {{$paypal->created_at}}
                            </td>
                            <td class="my-column">
                                {{$paypal->status}}
                            </td>
                            <td class="my-column">
                                @can('permission_delete')

                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteModal{{$paypal->id}}">delete</button>

                                <div class="modal fade" id="deleteModal{{$paypal->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deleteModalLabel">Are You Sure</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="POST" action="{{ url('admin/paypal/'.$paypal->id.'/delete')}}">
                                                @csrf
                                                @method('DELETE')
                                                <p>Are you sure.....</p>
                                                <button type="submit" class="btn btn-sm btn-danger">Yes</button>                        
                                            </form>
                                            <button type="button" class="btn btn-sm btn-secondary mt-2" data-dismiss="modal">No</button>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                                @endcan
                                
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@section('scripts')
@parent
<script>
 
</script>
@endsection