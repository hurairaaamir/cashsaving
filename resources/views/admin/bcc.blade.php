@extends('layouts.admin')


@section('content')

@can('permission_create')

     <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">

            <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#createBcc">create</button>

            <div class="modal fade" id="createBcc" tabindex="-1" role="dialog" aria-labelledby="updateBccLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="updateBccLabel">BCC</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{url('/admin/bcc/store')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label>Bcc:</label>
                                    <input type="text" class="form-control" name="bcc">
                                </div>
                                <div class="form-group">
                                    <label>image:</label>
                                    <input type="file" class="form-control" name="image">
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm mt-2">save</button>                        
                            </form>
                            <button type="button" class="btn btn-secondary btn-sm mt-2" data-dismiss="modal">Close</button>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>



            
        </div>
    </div>


@endcan
<div class="card">
    <div class="card-header">
        Bccs
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table  id="cardTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>
                            id
                        </th>
                        <th>
                            BCC
                        </th>
                        <th>
                            image
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach($bccs as $index=>$bcc)
                        @php 
                            $bcc_id=(isset($bcc->id) ? $bcc->id : '');
                        @endphp
                        <tr>
                            <td>
                                {{$index+1}}
                            </td>
                            <td>
                                {{isset($bcc->bcc) ? $bcc->bcc : ''}}
                            </td>
                            
                            <td style="width:20%;height:auto">
                                <img src="{{isset($bcc->image) ? $bcc->image : ''}}" style="width:100%;">
                            </td>
                            <td>
                                @can('permission_edit')
                                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#updateBcc{{$bcc_id}}">update</button>

                                    <div class="modal fade" id="updateBcc{{$bcc_id}}" tabindex="-1" role="dialog" aria-labelledby="updateBccLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="updateBccLabel">Update Bcc</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="{{url('/admin/bcc/'.$bcc_id.'/update')}}" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')
                                                        <label>Bcc:</label>
                                                        <input type="text" class="form-control" name="bcc" value="{{$bcc->bcc}}">
                                                        <label>image:</label>
                                                        <input type="file" class="form-control" name="image">
                                                        <button type="submit" class="btn btn-primary btn-sm mt-2">update</button>                        
                                                    </form>
                                                    <button type="button" class="btn btn-secondary btn-sm mt-2" data-dismiss="modal">Close</button>
                                                </div>
                                                <div class="modal-footer">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endcan

                                @can('permission_delete')

                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteModal{{$bcc_id}}">delete</button>

                                <div class="modal fade" id="deleteModal{{$bcc_id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deleteModalLabel">Are You Sure</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="POST" action="{{ url('admin/bcc/'.$bcc_id.'/delete')}}">
                                                @csrf
                                                @method('DELETE')
                                                <p>Are you sure.....</p>
                                                <button type="submit" class="btn btn-sm btn-danger">Yes</button>                        
                                            </form>
                                            <button type="button" class="btn btn-sm btn-secondary mt-2" data-dismiss="modal">No</button>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
 
</script>
@endsection