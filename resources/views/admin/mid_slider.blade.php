@extends('layouts.admin')


@section('content')

@can('permission_create')

     <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">

            <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#createmid_slider">create</button>

            <div class="modal fade" id="createmid_slider" tabindex="-1" role="dialog" aria-labelledby="updatemid_sliderLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="updatemid_sliderLabel">Top Slider</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{url('/admin/mid_slider/store')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label>image:</label>
                                    <input type="file" class="form-control" name="image">
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm mt-2">save</button>                        
                            </form>
                            <button type="button" class="btn btn-secondary btn-sm mt-2" data-dismiss="modal">Close</button>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>            
        </div>
    </div>


@endcan
<div class="card">
    <div class="card-header">
        Cards
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="cardTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>
                            id
                        </th>
                        <th>
                            image
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($mid_sliders as $index=>$mid_slider)
                    @php 
                        $imd_id=(isset($mid_slider->id) ? $mid_slider->id : '');
                    @endphp
                        <tr>
                            <td>
                                {{$index+1}}
                            </td>
                            <td style="width:20%;height:auto">
                                <img src="{{$mid_slider->image ? $mid_slider->image : ''}}" style="width:100%;">
                            </td>
                            <td>
                                @can('permission_delete')

                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteModal{{$imd_id}}">delete</button>

                                <div class="modal fade" id="deleteModal{{$imd_id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deleteModalLabel">Are You Sure</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="POST" action="{{ url('admin/mid_slider/'.$imd_id.'/delete')}}">
                                                @csrf
                                                @method('DELETE')
                                                <p>Are you sure.....</p>
                                                <button type="submit" class="btn btn-sm btn-danger">Yes</button>                        
                                            </form>
                                            <button type="button" class="btn btn-sm btn-secondary mt-2" data-dismiss="modal">No</button>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
 
</script>
@endsection