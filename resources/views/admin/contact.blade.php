@extends('layouts.admin')

@section('styles')
<style>
.my-column{
    min-width:200px;
}

</style>
@endsection

@section('content')
<div class="card">
    <div class="card-header">
        Contact Form Messages
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table  id="cardTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th class="my-column">
                            id
                        </th>
                        <th class="my-column">
                            Name
                        </th>
                        <th class="my-column">
                            Email
                        </th>
                        <th class="my-column">
                            Phone       
                        </th>
                        <th class="my-column">
                            subject
                        </th>
                        <th class="my-column">
                            description
                        </th>
                        <th class="my-column">
                            code
                        </th>
                        <th class="my-column">
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contacts as $contact)

                        <tr>
                            <td class="my-column">
                                {{$contact->id}}
                            </td>
                            <td class="my-column">
                                {{$contact->name}}
                            </td>
                            <td class="my-column">
                                {{$contact->email}}
                            </td>
                            <td class="my-column">
                                {{$contact->phone}}
                            </td>
                            <td class="my-column">
                                {{$contact->subject}}
                            </td>
                            <td class="my-column">
                                {{$contact->description}}
                            </td>
                            <td class="my-column">
                                {{$contact->code}}
                            </td>
                            <td class="my-column">
                                @can('permission_delete')

                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteModal{{$contact->id}}">delete</button>

                                <div class="modal fade" id="deleteModal{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deleteModalLabel">Are You Sure</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="POST" action="{{ url('admin/contact/'.$contact->id.'/delete')}}">
                                                @csrf
                                                @method('DELETE')
                                                <p>Are you sure.....</p>
                                                <button type="submit" class="btn btn-sm btn-danger">Yes</button>                        
                                            </form>
                                            <button type="button" class="btn btn-sm btn-secondary mt-2" data-dismiss="modal">No</button>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                                @endcan
                                
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@section('scripts')
@parent
<script>
 
</script>
@endsection