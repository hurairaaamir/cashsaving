@extends('layouts.admin')


@section('content')

@can('permission_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createCategory">create</button>

            <div class="modal fade" id="createCategory" tabindex="-1" role="dialog" aria-labelledby="createCategoryLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="createCategoryLabel">create category</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{url('/admin/category/store')}}">
                                @csrf
                                {{-- @method('PUT') --}}
                                <label>name:</label>
                                <input type="text" class="form-control" name="name">
                                <button type="submit" class="btn btn-primary btn-sm mt-2">Save</button>                        
                            </form>
                            <button type="button" class="btn btn-secondary btn-sm mt-2" data-dismiss="modal">Close</button>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        Categories
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="cardTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="10">
                            id
                        </th>
                        <th>
                            category name
                        </th>
                        <th>
                            forms
                        </th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach($categories as $index =>$category)
                    @php 
                        $category_id=(isset($category->id) ? $category->id : '');
                    @endphp
                        <tr data-entry-id="">
                            <td>
                                {{$index+1}}
                            </td>
                            <td>
                                {{isset($category->name) ? $category->name : ''}}
                            </td>
                            <td>
                                @can('permission_edit')
                                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#updateCategory{{$category_id}}">update</button>

                                    <div class="modal fade" id="updateCategory{{$category_id}}" tabindex="-1" role="dialog" aria-labelledby="updateCategoryLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="updateCategoryLabel">create category</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" action="{{url('/admin/category/'.$category_id.'/update')}}">
                                                        @csrf
                                                        @method('PUT')
                                                        <label>name:</label>
                                                        <input type="text" class="form-control" name="name" value="{{isset($category->name) ? $category->name : ''}}">
                                                        <button type="submit" class="btn btn-primary btn-sm mt-2">update</button>                        
                                                    </form>
                                                    <button type="button" class="btn btn-secondary btn-sm mt-2" data-dismiss="modal">Close</button>
                                                </div>
                                                <div class="modal-footer">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endcan

                                @can('permission_delete')

                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteModal{{$category_id}}">delete</button>

                                <div class="modal fade" id="deleteModal{{$category_id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deleteModalLabel">Are You Sure</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="POST" action="{{ url('admin/category/'.$category_id.'/delete')}}">
                                                @csrf
                                                @method('DELETE')
                                                <p>Are you sure. Deleting the category will delete all the cards belonging to that category</p>
                                                <button type="submit" class="btn btn-sm btn-danger">Yes</button>                        
                                            </form>
                                            <button type="button" class="btn btn-sm btn-secondary mt-2" data-dismiss="modal">No</button>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                                @endcan
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
 
</script>
@endsection