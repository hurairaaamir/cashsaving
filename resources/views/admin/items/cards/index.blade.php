@extends('layouts.admin')


@section('content')

@can('permission_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{url('admin/card/create')}}">
                create Card
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        Cards
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table id="cardTable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="10">
                            id
                        </th>
                        <th>
                            category 
                        </th>
                        <th>
                            title
                        </th>
                        <th>
                            phone no.
                        </th>
                        <th>
                            address
                        </th>
                        <th>
                            description
                        </th>
                        <th>
                            image
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cards as $index=>$card)
                        <tr data-entry-id="">
                            <td>
                                {{$index+1}}
                            </td>
                            <td>
                                {{ isset($card->category->name) ? $card->category->name : '' }}
                            </td>
                            <td>
                                {{ isset($card->title) ? $card->title : '' }}
                            </td>
                            <td>
                                {{ isset($card->phone) ? $card->phone : '' }}
                            </td>
                            <td>
                                {{ isset($card->address) ? $card->address : '' }}
                            </td>
                            <td>
                                {{ isset($card->description) ? $card->description : '' }}
                            </td>
                            <td style="width:20%;height:auto">                            
                                <img src="{{ isset($card->image) ? $card->image : '' }}" style="width:100%;">
                            </td>
                            <td>
                                @php 
                                    $card_id=(isset($card->id) ? $card->id : '');
                                @endphp
                                @can('permission_edit')
                                    <a class="btn btn-xs btn-info" href="{{url('/admin/card/'.$card_id.'/edit')}}">
                                        Edit
                                    </a>
                                @endcan
             
                                @can('permission_delete')

                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#deleteModal{{$card_id}}">delete</button>

                                <div class="modal fade" id="deleteModal{{$card_id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="deleteModalLabel">Are You Sure</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            <form method="POST" action="{{ url('admin/card/'.$card_id.'/delete')}}">
                                                @csrf
                                                @method('DELETE')
                                                <p>Are you Sure....</p>
                                                <button type="submit" class="btn btn-sm btn-danger">Yes</button>                        
                                            </form>
                                            <button type="button" class="btn btn-sm btn-secondary mt-2" data-dismiss="modal">No</button>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
