@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        Create Card
    </div>
    <div class="card-body">
        <form action="{{ url("/admin/card/store") }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">title:</label>
                <input type="text" name="title" class="form-control" value="{{old('title')}}">
                @if($errors->has('title'))
                    <p class="help-block red">
                        {{ $errors->first('title') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label >description:</label>
                <textarea name="description" class="form-control" value="{{old('description')}}">
                </textarea>
                @if($errors->has('description'))
                    <p class="help-block red">
                        {{ $errors->first('description') }}
                    </p>
                @endif
            </div>

            <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                <label for="name">phone number:</label>
                <input type="text" name="phone" class="form-control" value="{{old('phone')}}">
                @if($errors->has('phone'))
                    <p class="help-block red">
                        {{ $errors->first('phone') }}
                    </p>
                @endif
            </div>

            <div class="form-group">
                <label >address:</label>
                <input type="text" name="address" class="form-control" value="{{old('address')}}">
                @if($errors->has('address'))
                    <p class="help-block red">
                        {{ $errors->first('address') }}
                    </p>
                @endif
            </div>

            <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}" >
                <label >category:</label>
                <select class="form-control" name="category" value="{{old('category')}}">
                {{-- @if(old('category'))
                    @php 
                        $value= old('category');
                    @endphp
                    <option value='{{old('category')}}'" selected>{{$value}}</option>
                @else
                    <option selected></option>
                @endif --}}
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
                @if($errors->has('category'))
                    <p class="help-block red">
                        {{ $errors->first('category') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                <label >image:</label>
                <input type="file" class="form-control" name="image">
                @if($errors->has('image'))
                    <p class="help-block red">
                        {{ $errors->first('image') }}
                    </p>
                @endif
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
    </div>
</div>
@endsection

@section('css')

@endsection