@extends('layouts.admin')

@section('content')
<div class="card">
    <div class="card-header">
        Edit Card
    </div>
    @php 
        $card_id=(isset($card->id) ? $card->id : '');
    @endphp
    <div class="card-body">
        <form method="POST" action="{{url("/admin/card/".$card_id."/update") }}"  enctype="multipart/form-data">
            
            @csrf
            @method('PUT')

            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                <label for="name">title:</label>
                <input type="text" name="title" class="form-control" value="{{ isset($card->title) ? $card->title : '' }}">
                @if($errors->has('title'))
                    <p class="help-block red">
                        {{ $errors->first('title') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                <label >description:</label>
                <textarea name="description" class="form-control">
                    {{ isset($card->description) ? $card->description : '' }}
                </textarea>
                @if($errors->has('description'))
                    <p class="help-block red">
                        {{ $errors->first('description') }}
                    </p>
                @endif
            </div>
            <div class="form-group">
                <label for="name">phone number:</label>
                <input type="text" name="phone" class="form-control" value="{{ isset($card->phone) ? $card->phone : '' }}">
                @if($errors->has('phone'))
                    <p class="help-block red">
                        {{ $errors->first('phone') }}
                    </p>
                @endif
            </div>

            <div class="form-group">
                <label >address:</label>
                <input type="text" name="address" class="form-control" value="{{ isset($card->address) ? $card->address : '' }}">
                @if($errors->has('address'))
                    <p class="help-block red">
                        {{ $errors->first('address') }}
                    </p>
                @endif
            </div>
            <div class="form-group {{ $errors->has('roles') ? 'has-error' : '' }}" >
                <label >category:</label>
                <select class="form-control" name="category" value="{{old('category')}}">
                    
                    @foreach($categories as $category)    
                        @if($category->id==$card->category->id)
                        @else
                            <option value="{{ isset($category->id) ? $category->id : '' }}" >{{ isset($category->name) ? $category->name : '' }}</option>
                        @endif
                    @endforeach
                </select>
                @if($errors->has('category'))
                    <p class="help-block red">
                        {{ $errors->first('category') }}
                    </p>
                @endif
            </div>
            <div class="form-group ">
                <label >image:</label>
                <input type="file" class="form-control" name="image">
            </div>
            <div>
                <input class="btn btn-danger" type="submit" value="Save">
            </div>
        </form>
                    {{-- @foreach($categories as $category)
                    @php
                            $id=$category->id;
                            echo "id:".$id;
                            $second=$card->category->id;
                            echo "second id:".$second;
                        @endphp
    
                        @if($id==$second)
                            <option value="{{$category->id}}" >{{$category->name}}</option>
                        
                        @endif
                    @endforeach --}}

    </div>
</div>
@endsection

@section('css')

@endsection