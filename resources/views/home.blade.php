@extends('layouts.app')
@section('css')
<style>
.heading6{
  font-family: 'Source Sans Pro', sans-serif;
  font-size:45px;
  font-weight: 500;
  text-align: center;
  color:rgb(235, 235, 235);
}

@media only screen and (max-width:780px){
  .heading6{
    font-size:30px;
  }
}
@media only screen and (max-width:450px){
  .heading6{
    font-size:24px;
  }
}

</style>

@endsection

@section('js')

@endsection
@section('content')
        <div class="vimeo-wrapper">    
            <iframe src="https://player.vimeo.com/video/371533421?loop=1"
             width="700"
            frameborder="0" webkitallowfullscreen mozallowfullscreen
             >
             </iframe>
        </div>
        <div class="big-section">
          <h1>CashSavings4</h1>
          <h1>Consumers</h1>
        </div>
        {{-- <div class="vimeo-wrapper">    
            <iframe src="https://player.vimeo.com/video/374590923?loop=1"
             width="700"
            frameborder="0" webkitallowfullscreen mozallowfullscreen
             >
             </iframe>
        </div> --}}
<section class="text-center">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <div class="heading2">
        SAVE YOUR CASH AND USE BCC TOWARDS EVERYDAY
        PURCHASES A POWERFUL ADDED VALUE LOYALTY
          PROGRAM
      </div>
    </div>
  </div>
</section>

    <section id="account-activation" class="section-11">
      <div class="container" >
        <div class="row mt-3">
          <div class="col-md-12">
            <div id="client-slider" class="owl-carousel owl-theme mt-4">
              @foreach($top_sliders as $top_slider)
              <div class="client-item">
                <div class="img-contain">
                  <img
                    src="{{$top_slider->image}}"
                    class="img-responsive"
                    alt=""
                  />
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>




 <section id="signup">
      <div class="container">
        <div class="row">
          {{-- <div class="signup">
            <h2 class="heading1">
              Create Your Free Account NOW with 100$ of cash savings by
              DOWNLOADING the BankCoinCredit App Off the Apple I store or the Android App Store(coming soon) Off the Google play store Or open up a free account off the
              <a href="https://bankcoincredit.com/">BankCoinCredit.com</a>
            </h2>
          </div> --}}
          <div class="update-signup">
            <h1>Sign UP for your FREE ACCOUNT HERE on the BCC Market Place Website</h1>
              <div class="clickHererow">
              <div class="clickHere">
                <a href="https://bankcoincredit.com/index.php/home/signup_step_1" target="_blank" class="text-uppercase text-center px-4 py-2 mx-auto mt-4 hover-link" >click here and signup</a>
              </div>
              </div>
            <h2 class="heading6" >And receive YOUR $1,000 Cash Savings credited to you’re a/c ready to spend.. and when you spend 200 Plus a month then your on your way to receive another 1000BCC deposited into your a/c</h2>
            {{-- <h1 style="color:white;margin-bottom:30px;"></h1> --}}
            <p>This is a <b>GENUINE OFFER</b> to help meet the needs of the people and to help STIMULATE the Australian Economy</p>
            <div class="clickHererow">
              <div class="clickHere">
                <a href="https://bankcoincredit.com/index.php/home/signup_step_1" target="_blank" class="text-uppercase text-center px-4 py-2 mx-auto mt-4 mb-3 hover-link" >click here and signup</a>
              </div>
              </div>
            <p>Then come back and DOWNLOAD the BCC APP To make purchases or sales with our <b>REVOLUTIONARY MOBILE Digital Payment System in BCC</b> (digital currency) And to keep track of your balance</p>
            <p>When you Sign UP on the BCC Website it will <b>activate your BCC APP</b>, then simply sign in with your details from the website</p>
          </div>
        </div>
      </div>
    </section>
    <div class="stores">
      <div class="store-logo" style="margin-top:30px;">
        <a href="https://apps.apple.com/us/app/bank-coin-credit/id1460595709" target="_blank">
          <img src="/images/Imagesss/img/apple.png" >
        </a>
      </div>
      <div class="store-logo" style="margin-top:33px">
      {{-- <div class="store-coming"></div> --}}
        <a href="https://play.google.com/store/apps/details?id=com.vbarter.mobile.bcc" target="_blank">
          <img src="/images/Imagesss/img/google.png" >
        </a>
      </div>
    </div>


    <section class="section-13">
      <div class="container">
{{--         
          <div class="heading4">
              Don't have an Apple I phone, then open up a free account Off
              the <a href="https://bankcoincredit.com/"> www.Bankcoincredit.com</a> website
          </div> --}}
          <div class="row">
            <div style="display:block;">
            <a href="https://bankcoincredit.com/" target="_blank">
              <img src="/images/Imagesss/img/bankcoin.png" style="width:100%;">
            </a>
            </div>
          </div>
      </div>
    </section>
    <!-- ++++++++++++ Section - 1 ++++++++++++++++++++ -->


    <!-- ++++++++++++ Section - 3 ++++++++++++++++++++ -->
    <section id="account-activation">
      <div class="container">
        <div class="row">
          <div class="signup-content">
            <h2 class="heading3">
              Your Account Will Be Activated With $1,000 Of cash
              Savings Ready To Spend In Our Growing Network
              Within 24 Hours
            </h2>
          </div>
        </div>
        <div class="row mt-3">
          <div class="col-md-12">
            <div id="bottom-slider" class="owl-carousel owl-theme">
            @foreach($mid_sliders as $mid_slider)
              <div class="client-item">
                <div class="img-contain">
                  <img
                    src="{{$mid_slider->image}}" alt=""
                  />
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- ++++++++++++ Section - 3 ++++++++++++++++++++ -->

    <!-- +++++++++++  Section - 4 +++++++++++++++++++++ -->
    <section id="saving-cards">
      <div class="container">
        <div class="row">
          <div class="container">
            <div class="row">
              {{-- <div class="signup-content"> --}}
                <h2 class="heading1">
                  WHERE I CAN SPEND AND SAVE CASH USING MY APP, OR BCC SAVING
                  CARDS?
                </h2>
              {{-- </div> --}}
            </div>
              <div class="clickHererow">
                <div class="clickHere1">
                  <a
                    href="{{url('/product')}}"
                    role="button" target="_blank"
                    class="text-uppercase text-center px-4 py-2 mx-auto mt-4 hover-link" 
                  >
                    click here 
                  </a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- +++++++++++  Section - 4 +++++++++++++++++++++ -->

    <!-- +++++++++++++ Section 5 ++++++++++++++++++++++ -->
    <section id="section5">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div id="saving-slider" class="owl-carousel owl-theme mt-4">
              @foreach($bccs as $bcc)
                <div class="saving-item">
                <div class="img-contain">
                  <img src="{{$bcc->image}}" alt=""/>
                  <div class="overlay">
                    <h2 class="text-center">
                      Use {{$bcc->bcc}} BCC
                    </h2>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
        <div class="clickHererow">
            <div class="clickHere1">
              <a
                href="{{url('/sales')}}"
                role="button" target="_blank"
                class="text-uppercase text-center px-4 py-2 mx-auto mt-4 hover-link" 
              >
                click here 
              </a>
            </div>
        </div>
      </div>
    </section>
    <!-- +++++++++++++ Section 5 ++++++++++++++++++++++ -->
    <!-- +++++++++++++ Section 5 ++++++++++++++++++++++ -->
    <section id="section9">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div id="deadground-slider" class="owl-carousel owl-theme mt-4">
              @foreach($last_sliders as $last_slider)
              <div class="client-item">
                <div class="img-contain">
                  <img src="{{$last_slider->image}}" class="img-responsive" alt=""/>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- +++++++++++++ Section 5 ++++++++++++++++++++++ -->
 <!-- +++++++++++++++++++++++++++ Section 7 ++++++++++++++++ -->
    <section id="section7">
      <div class="container-fluid">
        <div class="row">
          <div class="signup-content mx-auto mb-3 ">
            <h2 class="heading1">
              WANT MORE SAVINGS ADDED TO YOUR BCC APP OR CARD?
            </h2>
          </div>
        </div>
        <div class="row">
          <div class="bg-img"></div>
        </div>
         <div class="clickHererow">
                <div class="clickHere2">
                  <a
                    href="{{url('/payment')}}"
                    role="button" target="_blank"
                    class="text-uppercase text-center px-4 py-2 mx-auto mt-4 hover-link" 
                  >
                    click here 
                  </a>
                </div>
            </div>
      </div>
       
    </section>
    <!-- +++++++++++++++++++++++++++ Section 7 ++++++++++++++++ -->
    <!-- ++++++++++++++++++++++++ Section 6 +++++++++++++++ -->

    <section id="section6">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h2 class="text-uppercase heading5 text-center">
              Want to sell stuff on the BCC Market Place website?
            </h2>
            <div class="img-contain2">
              <img src="/images/Imagesss/img/sale.jpg" alt="" />
            </div>
            <p class="mt-4 text-center">
              If you have stuff hanging around and you want to move it on, or you have a service or a product you would like to promote, it could be mowing lawns, an ironing service or something you can do to earn extra income get yourself up on the BCC market place by opening up a free account as an individual.
            </p>
          </div>
          <div class="col-md-6">
            <h2 class="text-uppercase mb-2 heading5 text-center"style="margin-top:30px;">
              BCC Updates 
            </h2>
            <div class="img-contain2">
              <img src="/images/Imagesss/img/mobile.jpg" alt=""/>
            </div>
            <p class="mt-4 text-center">
              With BCC you are notified every time new products or services are added to the BCC Network this includes personal services, cafes, restaurants, bars, accommodation, rentals, entertainment and retail.
            </p>
          </div>
        </div>
      </div>
    </section>
    <!-- ++++++++++++++++++++++++ Section 6 +++++++++++++++ -->

   
    <!-- +++++++++++++++++++++++++++ Section 8 ++++++++++++++++ -->

    <section id="section8">
      <div class="container-fluid">
        <div class="row">
          <div class="signup-content mx-auto mb-3">
            <h2 class="heading1" style="color:black!important">
                    DINNER PRESENTATION TO EMPOWER MERCHANTS AND CONSUMERS COMING 
            </h2>
          </div>
        </div>
        <div class="row">
          <div class="bg-img"></div>
        </div>
        <div class="row">
          <a href="http://bccAgents.biz" role="button" class="text-center px-4 py-2 mx-auto bcc-link" target="_blank">
            <u>BCC Business Licensees's Opportunities</u>
          </a>
        </div>
      </div>
    </section>
    <!-- +++++++++++++++++++++++++++ Section 8++++++++++++++++ -->

     <!-- --------------------------- Contact Section-------- -->
<footer class="home-footer">
  <section class="tab-section">
    <div class="findus">
      <h1>Find <span class="mycolor">Us<span></h1>
    </div>
    <div class="location">
      <h1>OUR LOACTION</h1>
      <div style="max-width:430px;"><i class="fa fa-map-marker mycolor" aria-hidden="true"></i> Suit 21804 HILTON HOTEL 6 Orchid Avenue Surfers Paradise Gold Coast Queensland,4217 Australia</div>
      <div class="ml-2"></div>
    </div>
    <div class="location">
      <h1>CALL US</h1>
      <div><i class="fa fa-phone mycolor" aria-hidden="true"></i> +61 481 348 230</div>
    </div>
    <div class="location">
      <h1>MAIL US</h1>
      <div><i class="fas fa-envelope mycolor"></i> bankcoincredit@gmail.com</div>
    </div>
  </section>

     <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div id="map" class="row">
              <div class="mapouter wow fadeInLeft col-md-12">
                <div class="gmap_canvas">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3522.774248078155!2d153.4273773143319!3d-28.000802446199792!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b9105725773fa53%3A0xc2d5ebb3a1b2a319!2sOrchid%20Ave%2C%20Surfers%20Paradise%20QLD%204217%2C%20Australia!5e0!3m2!1sen!2s!4v1588401380716!5m2!1sen!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="contact-form">
         
              
              <form id="contactus">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <input type="text" placeholder="Name" class="form-control contact-feild" name="name" />
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input type="email" placeholder="Email Address" class="form-control contact-feild" name="email" required/>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input  type="text" placeholder="Contact Number" class="form-control contact-feild" name="phone"/>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <input type="text" placeholder="subject" name="subject" class="form-control contact-feild" />
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                    <textarea class="form-control contact-feild" placeholder="description" name="description" id="" cols="18" rows="5" required></textarea>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="form-group">
                      <input type="text" placeholder="Code" name="code" class="form-control contact-feild"/>
                    </div>
                  </div>
                </div>
                    <button  class="btn-submit" id="contact-btn" style="cursor:pointer;">Send</button>
                  </div>
              </form>
              <div id="category_result"></div>
          </div>
        </div>
      </div>
    </section>
</footer>


<script>
$('#contactus').on('submit',function(event){
        event.preventDefault();
        document.getElementById('contact-btn').innerHTML='sending....';
        document.getElementById('contact-btn').style.backgroundColor='#010f2e';
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var html='';
        $.ajax({
            method:"post",
            url:'{{url('contact')}}',
            data:new FormData(this),
            contentType:false,
            cache:false,
            processData:false,
            datatype:'json',
            success:function(data){
               html +='<div style="padding:8px 0px;font-size:15px;background-color:#09072F;margin-top:5px;border-radius:5px;color:white;display:flex;justify-content:center;align-items:center;"><h6> <i class="far fa-check-circle" style="font-size:25px;margin-right:4px;transform:translateY(4px);"></i>Message successfully sent thank you for contacting us!</h6></div>';
               $("#category_result").html(html);

               $("#contactus")[0].reset();
              document.getElementById('contact-btn').innerHTML='Send';
              document.getElementById('contact-btn').style.backgroundColor='#002066';
            },
            error:function(data) {
             
              html +='<div style="padding:8px 0px;font-size:15px;background-color:#09072F;margin-top:5px;border-radius:5px;color:white;display:flex;justify-content:center;align-items:center;"><h6> <i class="far fa-check-circle" style="font-size:25px;margin-right:4px;transform:translateY(4px);"></i>Message successfully sent thank you for contacting us!</h6></div>';
              document.getElementById('category_result').innerHTML=html;
              $("#contactus")[0].reset();
              document.getElementById('contact-btn').innerHTML='Send';
              document.getElementById('contact-btn').style.backgroundColor='#002066';
            }

        });
        
      
  

});
</script>
                {{-- $('#ad-data').html(data); --}}

    <!-- --------------------------- Contact Section-------- -->

    <!-- ------------------------------- Footer -->

    
    <!-- ------------------------------- Footer -->


@endsection

@section('scripts')

  

  <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     



  
  <script src="https://player.vimeo.com/api/player.js"></script>
@parent

@endsection