<footer class="bottom-footer">
    <div class="usefull">
        <h1>USEFUL LINKS</h1>
        <p><a href="{{ url('/home') }}">Home</a></p>
        <p><a href="{{ url('/product') }}">Products</a></p>
        <p><a href="{{ url('/sales') }}">Sales</a></p>
        <p><a href="{{ url('/payment') }}">Payment</a></p>
        <p><a href="http://bccagents.biz/">BCC Opportunities</a></p>

    </div>
    <div class="usefull">
        <h1>SOCIAL MEDIA</h1>
        <p><a href="#"><i class="fab fa-facebook-square"></i> Facebook</a></p>
        <p><a href="#"><i class="fab fa-twitter"></i> Twitter</a></p>
        <p><a href="#"><i class="fab fa-youtube"></i> YouTube</a></p>
        <p><a href="#"><i class="fab fa-google"></i> Google+</a></p>
    </div>
    <div class="usefull">
        <h1>PAYMENTS</h1>
        <div style="width:200px;"><img src="/images/Imagesss/img/paypal-logo.png" style="width:100%;height:100%;"></div>
    </div>
</footer>
<footer class="bead-bottom">
    <div>© 2020 Bank Coin Credit. All Rights Reserved.</div>
    <div class="u-rigrex"><a href="https://rigrex.com/" target="_blank">Developed by :<img src="/images/Imagesss/img/rigrex.png" class="rigrex"> RigRex | Creators Of Tomorrow</a><div>
<footer>