{{-- <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
                @if (Route::has('login'))
                    @auth
                        <li class="nav-item active">
                            <a href="{{ url('/home') }}" class="nav-link">Home</a>
                        </li>
                        <li class="nav-item active">
                            <a href="{{ url('/product') }}" class="nav-link">products</a>
                        </li>
                        <li class="nav-item active">
                            <a href="{{ url('/sales') }}" class="nav-link">sales</a>
                        </li>
                        <li class="nav-item active">
                            <a href="{{ url('/payment') }}" class="nav-link">payment</a>
                        </li>
                        <li class="nav-item active">
                            <a href="{{ url('/logout') }}" class="nav-link">logout</a>
                        </li>
                    @else
                        <li class="nav-item active">
                            <a href="{{ route('login') }}" class="nav-link">Login</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="nav-item active">
                                <a href="{{ route('register') }}" class="nav-link">Register</a>
                            </li>
                        @endif
                    @endauth
            @endif
    </ul>
  </div>
</nav> --}}

<nav class="nav-inactive nav-color" >
    <div class="logo">
        {{-- <h4>LOGO</h4> --}}
        <div class="img-con">
            <img src="/images/sales/logo.jpg">
        </div>
    </div>
    <ul class="nav-links">
        <li>
            <a href="{{ url('/home') }}">Home</a>
        </li>
        <li>
            <a href="{{ url('/product') }}">Products</a>
        </li>
        <li>
            <a href="{{ url('/sales') }}">Sales</a>
        </li>
        <li>
            <a href="{{ url('/payment') }}">Payments</a>
        </li>
         <li>
            <a href="http://bccagents.biz/">BCC Opportunities</a>
        </li>
    </ul>
    <div class="email" >
        <h5><i class="far fa-envelope meMail"></i>bankcoincredit@gmail.com</h5>
    </div>
    <div class="burger">
        <div class="line1"></div>
        <div class="line2"></div>
        <div class="line3"></div>
    </div>
</nav>
