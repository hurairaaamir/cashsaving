<aside class="main-sidebar sidebar-dark-primary elevation-4" style="min-height: 917px;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="/images/sales/logo.jpg" class="admin-logo">
        <span class="brand-text font-weight-light">ADMIN PANEL</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ url("admin") }}" class="nav-link {{ request()->is('admin') ? 'active' : '' }}">
                        <i class="fas fa-tachometer-alt"></i>
                        <p>
                            <span>{{ trans('global.dashboard') }}</span>
                        </p>
                    </a>
                </li>
                {{-- @can('user_management_access') --}}

                <li class="nav-item">
                    <a href="{{ url("/admin/paypal") }}" class="nav-link {{ request()->is('admin/paypal') || request()->is('admin/paypal/*') ? 'active' : '' }}">
                        <i class="fab fa-cc-paypal"></i>
                        <p>
                            <span>Paypal Record</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url("/admin/payment") }}" class="nav-link {{ request()->is('admin/payment') || request()->is('admin/payment/*') ? 'active' : '' }}">
                        <i class="fas fa-paragraph"></i>
                        <p>
                            <span>Payment Record</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url("/admin/contact") }}" class="nav-link {{ request()->is('admin/contact') || request()->is('admin/contact/*') ? 'active' : '' }}">
                        <i class="fas fa-file-signature"></i>
                        <p>
                            <span>Contact Messages</span>
                        </p>
                    </a>
                </li>
                    {{-- <li class="nav-item has-treeview {{ request()->is('admin/permissions*') ? 'menu-open' : '' }} {{ request()->is('admin/roles*') ? 'menu-open' : '' }} {{ request()->is('admin/users*') ? 'menu-open' : '' }}">
                        <a class="nav-link nav-dropdown-toggle">
                            <i class="fas fa-users">

                            </i>
                            <p>
                                <span>{{ trans('global.userManagement.title') }}</span>
                                <i class="right fa fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('permission_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                                        <i class="fas fa-unlock-alt">

                                        </i>
                                        <p>
                                            <span>{{ trans('global.permission.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('role_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                                        <i class="fas fa-briefcase">

                                        </i>
                                        <p>
                                            <span>{{ trans('global.role.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('user_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                                        <i class="fas fa-user">

                                        </i>
                                        <p>
                                            <span>{{ trans('global.user.title') }}</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('product_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.products.index") }}" class="nav-link {{ request()->is('admin/products') || request()->is('admin/products/*') ? 'active' : '' }}">
                            <i class="fas fa-cogs">

                            </i>
                            <p>
                                <span>{{ trans('global.product.title') }}</span>
                            </p>
                        </a>
                    </li>
                @endcan --}}

                {{-- *************************************************** items toggle ******************************************* --}}
                <li class="nav-item has-treeview {{ request()->is('admin/cards*') ? 'menu-open' : '' }} {{ request()->is('admin/categories*') ? 'menu-open' : '' }}" style="cursor:pointer">
                    <a class="nav-link nav-dropdown-toggle">
                        <i class="fab fa-product-hunt"></i>
                        <p>
                            <span> Products </span>
                            <i class="right fa fa-angle-left"></i>
                        </p>
                    </a>
                        <ul class="nav nav-treeview">
                            @can('permission_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.cards") }}" class="nav-link {{ request()->is('admin/cards') || request()->is('admin/cards/*') ? 'active' : '' }}">
                                        <i class="far fa-credit-card"></i>
                                        <p>
                                            <span>cards</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('role_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.categories") }}" class="nav-link {{ request()->is('admin/categories') || request()->is('admin/categories/*') ? 'active' : '' }}">
                                        <i class="fab fa-cuttlefish"></i>
                                        <p>
                                            <span>categories</span>
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            
                        </ul>
                </li>
                {{-- <li class="nav-item">
                    <a href="{{ url("/admin/product") }}" class="nav-link {{ request()->is('admin/product') || request()->is('admin/product/*') ? 'active' : '' }}">
                        <p>
                            <i class="fab fa-product-hunt"></i>
                            <span>products</span>
                        </p>
                    </a>
                </li> --}}
                <li class="nav-item">
                    <a href="{{ url("/admin/bcc") }}" class="nav-link {{ request()->is('admin/bcc') || request()->is('admin/bcc/*') ? 'active' : '' }}">
                        <i class="fas fa-bold"></i>
                        <p>
                            <span>BCC sliders</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url("/admin/top_slider") }}" class="nav-link {{ request()->is('admin/top_slider') || request()->is('admin/top_slider/*') ? 'active' : '' }}">
                        <i class="fas fa-long-arrow-alt-up"></i>
                        <p>
                            <span>top slider</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url("/admin/mid_slider") }}" class="nav-link {{ request()->is('admin/mid_slider') || request()->is('admin/mid_slider/*') ? 'active' : '' }}">
                        <i class="fas fa-long-arrow-alt-right"></i>
                        <p>
                            <span>mid slider</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url("/admin/last_slider") }}" class="nav-link {{ request()->is('admin/last_slider') || request()->is('admin/last_slider/*') ? 'active' : '' }}">
                        <i class="fas fa-long-arrow-alt-down"></i>
                        <p>
                            <span>last slider</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <i class="fas fa-sign-out-alt"></i>
                        <p>
                            <span>{{ trans('global.logout') }}</span>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>