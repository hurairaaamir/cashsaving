{{-- <link rel="stylesheet" href="./css/bootstrap.min.css" />
    <!-- Magnific Popup core CSS file -->
    <link rel="stylesheet" href="../css/magnific-popup.css" /> --}}
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="/css/owl.theme.default.min.css" />
    <!-- Google Fonts -->
    <link
      href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,500,600,700&amp;display=swap"
      rel="stylesheet"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans:100,200,300,400,600,700&amp;display=swap"
      rel="stylesheet"
    />
    <!-- Google Fonts -->
    <!-- Main css -->
    <link rel="stylesheet" href="/css/main.css" />