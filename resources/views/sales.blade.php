@extends('layouts.app')


@section('content')
<section>
        <div class="sal-top">
            <div class="h1-mar">
                <h3>CashSaving4Consumers</h3>    
                <h1>One BCC has the same value and purchasing Power as One AUS dollar</h1>
            </div>
        </div>
    <div class="sal-heading1 gr-col">
        <div class="heading">
            <h3>Purchasing with Your BCC App gives you incredible leverage</h3>
        </div>
        <div class="h1-mar">
            <h1>IMPROVE YOUR LIFESTYLE</h1>
        </div>
    </div>
</section>

<section class="sal-sec-2">
        <div class="img-con"><img src="/images/sales/goodlife1.jpg"></div>
        <div class="img-con"><img src="/images/sales/boat1.jpg"></div>
        <div class="img-con"><img src="/images/sales/boat2.jpg"></div>
        <div class="img-con"><img src="/images/sales/truck.jpg"></div>
</section>

<div class="sal-heading1">
    <div class="h4-mar">
        <h1>The Australian dream of owning your own home is now possible using
BCC towards the purchase (coming)</h1>
    </div>
</div>

<section class="sal-sec-5">    
        <div class="img-con"><img src="/images/sales/house1.jpg"></div>
        <div class="img-con"><img src="/images/sales/house2.jpg"></div>
        <div class="img-con"><img src="/images/sales/house3.jpg"></div>
</section>

<div class="sal-heading1">
    <div class="h4-mar">
        <h1>Purchasing a vehicle, boat, a holiday or an income producing business using BCC towards the purchase are just some of the benefits of having Cash Savings <i>(BCC)</i> LOADED and ready to purchase on your BCC App. These items are coming</h1>
    </div>
</div>

<section class="sal-sec-4">
        <div class="img-con"><img src="/images/sales/car1.jpg"></div>
        <div class="img-con"><img src="/images/sales/car2.jpg"></div>
        <div class="img-con"><img src="/images/sales/car3.jpg"></div>
</section>

<section class="banner">
    <div class="child-1">
        <div class="img-con"><img src="/images/sales/amazed.jpg"></div>
    </div>
</section>



<section class="sal-sec-3">
    <h1>CashSavings4Consumers happens 2 ways</h1>
    <div class="sec-3-lower">
        <div class="child">
            <div class="child1">
                <span>1</span>
            </div>
            <div class="child2">
                <h3>You can purchase BCC</h3>
            </div>        
        </div>
        <div class="child">
            <div class="child1">
                <span>2</span>
            </div>
            <div class="child2">
                <h3>Or You can sell or offer a service in the BCC Market Place and earn BCC</h3>
            </div>        
        </div>
        <div class="child">
            <div class="child1">
                <span>3</span>
            </div>
            <div class="child2">
                <h3>Open a free account at  <a href="https://bankcoincredit.com/index.php/home/signup_step_1">BANKCOINCREDIT.COM</a></h3>
            </div>        
        </div>
    <div>
    <div class="saleHere">
        <div class="salehereinner">
            <a href="https://bankcoincredit.com/index.php/home/signup_step_1" target="_blank" class="text-uppercase text-center px-4 py-2 mx-auto mt-4 hover-link" >click here and signup</a>
        </div>
    </div>
</section>







<section class="banner">
    <div class="child-1">
        <div class="img-con">
            <a href="https://bankcoincredit.com/" target="_blank">
                <img src="/images/Imagesss/img/bankcoin.png">
            </a>
        </div>
    </div>
</section>

<section class="sal-sec-6">
    <a href="{{route('payment',['cost' => '30' ])}}">
        <div class="button">
            <span class="dollar">$30 buys</span> you $1,200 of Cash <span class="saving">Savings</span>
        </div>
    </a>
    {{-- <a href="{{route('payment',['cost' => '60'])}}">
        <div class="button">
            <span class="dollar">$60 buys </span>you $4,000 of Cash <span class="saving">Savings</span>
        </div>
    </a> --}}
</section>

<section class="banner-card">
    <div class="child-1">
        <div class="img-con"><img src="/images/sales/card.jpg"></div>
    </div>
</section>


<section  class="banner2">
    <div class="sal-heading1">
        <div class="h4-mar">
            <h2>CashSavings4Consumers Terms and Conditions</h2>
        </div>
    </div>

    <div class="sal-sec-1">
        
        <p class="para1"><b>IT’S SIMPLE… IT’S FREE TO JOIN, THERE ARE NO APPLICATION FEES, NO MONTHLY FEES and NO CONTRACTS.</b></p>

        <p><span class="lightblue">If you have not yet claimed your $1,000 of CASH SAVINGS Click the Banner below that directs to the BCC website</span>
        and get all the information required, sign up, then download the BCC App,<i>(it’s like having
        your digital currency payment system that puts you in TOTAL CONTROL)</i>
        Consumers can save up to 100% in cash savings on purchases throughout our growing BCC
        Network. Conserving your cash for other things.
        </p>        
    </div>
    <div class="saleHere">
        <div class="salehereinner">
            <a href="https://bankcoincredit.com/index.php/home/signup_step_1" target="_blank" class="text-uppercase text-center px-4 py-2 mx-auto mt-4 hover-link" >click here and signup</a>
        </div>
    </div>
    <div class="sal-sec-2">
        <div class="stimulate">
            <p>
            BCC(digital currency) is about adding more currency
            into the community to <b>help stimuate our economy</b>
            </p>
        </div>
    </div>
</section>

<div class="head-para">
    <h1>Making a purchase with your BCC App is show and GO in real time</h1>
    <p>Most merchants will display the BCC logo on their shop front.</p>
</div>

<div class="bcc-section">
    <div class="img-con"><img src="/images/sales/bcc.png"></div>
    <h1>BONUS OFFER</h1>
</div>
<section class="lower">
    <div class="lower-sec-1" style="padding-top:20px;">
        <div class="child1">
            <span>1</span>
        </div>
        <div class="child2">
            <span>Purchase today</span>
        </div>
    </div>
    <div class="lower-sec-1">
        <div class="child1">
            <span>2</span>
        </div>
        <div class="child2">
            <span>Claim your $1,000 of Cash Saving (BCC)</span>
        </div>
    </div>
    <div class="lower-sec-1">
        <div class="child1">
            <span>3</span>
        </div>
        <div class="child2">
            <span>And receive a <b>BONUS of up to $1,200 </b> in Cash Savings</span>
        </div>
    </div>
    <div class="lower-sec-1">
        <div class="child1">
            <span>4</span>
        </div>
        <div class="child2">
            <span>That’s $2,200 of Cash Savings loaded onto your BCC App ready to spend</span>
        </div>
    </div>
    <div class="lower-sec-1">
        <div class="child1">
            <span>5</span>
        </div>
        <div class="child2">
            <span><b>This is a LIMITED Time offer</b></span>
        </div>
    </div>
    <div class="lower-sec-1">
        <div class="child2">
            <span class="what-say">what are we saying?</span>
        </div>
    </div>
    <div class="lower-sec-1">
        <div class="child1">
            <span>1</span>
        </div>
        <div class="child2">
            <span>With a $30 purchase you will receive $1,000 plus $1,200 in Cash Savings that totals <b class="relative-size">$2,200</b></span>
        </div>
    </div>
    {{-- <div class="lower-sec-1">
        <div class="child1">
            <span>2</span>
        </div>
        <div class="child2">
            <span>With a $60 purchase you will receive $1,000 plus $4,000 in Cash Savings that totals <b class="relative-size">$5,000</b> in BCC (digital currency) ready to go. BUY NOW!</span>
        </div>
    </div> --}}
            
    </div> 
    
</section>
 <div class="lower-sec-2"><h1>Purchase NOW and INCREASE you’re 
spending while this BONUS is on OFFER 
Click the banner below</h1></div>
<section class="sal-sec-6" style="margin-bottom:50px;">
    <a href="{{route('payment',['cost' => '30' ])}}">
        <div class="button">
            <span class="dollar">$30 buys</span> you $2,200 of Cash <span class="saving">Savings</span>
        </div>
    </a>
    {{-- <a href="{{route('payment',['cost' => '60'])}}">
        <div class="button">
            <span class="dollar">$60 buys </span>you $5,000 of Cash <span class="saving">Savings</span>
        </div>
    </a> --}}
</section>

@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('/css/sales.css')}}">
@endsection

