@extends('layouts.app')


@section('content')
<div class="pro_h">
    <h1>WHERE CONSUMERS CAN SPEND BCC IN OUR GROWING NETWORK</h1>
    <h2>CashSavings4Consumers</h2>
    <p>Our <b>Foundation Merchants</b> in Surfers Paradise accepting BCC, <i>digital currency</i></p>
</div>
<div class="container">
    <div class="row" style="margin:0px 20px;">
        <div class="col-md-6">
            <div class="pro-con">
                <img src="/images/products/product1.jpg" >
            </div>
        </div>
        <div class="col-md-6">
            <div class="pro-con">
                <img src="/images/products/product2.jpg">
            </div>
        </div>
        <div class="col-md-6">
            <div class="pro-con">
                <img src="/images/products/product3.jpg">
            </div>
        </div>
        <div class="col-md-6">
            <div class="pro-con">
                <img src="/images/products/product4.jpg">
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="pro_h2">
            <h2>To make an appointment or a reservation Phone the Merchant</h2>
            <p>To make a purchase over the Phone with your BCC App contact the Merchant, give your name or business name you registered with BCC, they will select <b>POST a SALE</b> in their BCC App, description, and the purchase amount in BCC and process the sale in real time.</p>
            <p>This transaction is for <b>BCC only</b>, then check your balance by refreshing the page with the icon top right</p>
        </div>
    </div>
</div>

<div class="pro-container mb-5" >
    <div class="cate">
        <select class="form-control" onchange="getbycategory()" id="catSelect">                
            @foreach($categories as $category)
                <option value="{{$category->id}}">
                        {{$category->name}}
                </option>
            @endforeach
        </select>
    </div>
    <div class="flex-container" id="get-data">
    </div>
</div>

@endsection


@section('js')

    <script>
    document.addEventListener("DOMContentLoaded", function(event) { 
        getbycategory();
    });
    function getbycategory(){
        var select= document.getElementById("catSelect");
        var id = select.options[select.selectedIndex].value;
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type:'GET',
                url:`/getByCategory/${id}`,
                datatype:'json',
                success:function(data) {
                    document.getElementById('get-data').innerHTML=`${data.data}`;
                }
            });
    }
    getbycategory();
</script>
@endsection






