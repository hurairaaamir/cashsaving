@extends('layouts.app')
<script
    src="https://www.paypal.com/sdk/js?client-id=AVxwb6cNVGiHNO1Rgfy2WydokIF0Ut7f9jU0Jh6v3LcTi_M_-cRBU7tAqX8qlXVCn54VBLQYvlPjv81d&currency=AUD" >
</script>
{{-- AVxwb6cNVGiHNO1Rgfy2WydokIF0Ut7f9jU0Jh6v3LcTi_M_-cRBU7tAqX8qlXVCn54VBLQYvlPjv81d
AVxwb6cNVGiHNO1Rgfy2WydokIF0Ut7f9jU0Jh6v3LcTi_M_-cRBU7tAqX8qlXVCn54VBLQYvlPjv81d --}}
{{-- AVxwb6cNVGiHNO1Rgfy2WydokIF0Ut7f9jU0Jh6v3LcTi_M_-cRBU7tAqX8qlXVCn54VBLQYvlPjv81d --}}
@section('content')

<div id="paypalflash">
  <div id="flash">
    <h5><i class="far fa-check-circle" style="font- size:30px;"></i> Transaction Made Successfully</h5>
  </div>
</div>

@if($payment)

<div style="display:flex;justify-content:center;align-items:center;flex-flow:column nowrap;margin-top:28vh;margin-bottom:28vh;">
  <div class="paypal-btn">
    <div id="paypal-button-container"></div>
  </div>
</div>

@endif


<script>
        paypal.Buttons({

            createOrder: function(data, actions) {
            // This function sets up the details of the transaction, including the amount and line item details.
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '{{$payment}}'
                    }
                }]
            });
            },

            onApprove: function(data, actions) {
            // This function captures the funds from the transaction.
            return actions.order.capture().then(function(details) {
              // This function shows a transaction success message to your buyer.
              sendAjax(details);
              alert('Transaction completed by ' + details.payer.name.given_name);
              document.getElementById('flash').style='display:block';
            });
        }
        }).render('#paypal-button-container');

      function sendAjax(details){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

              $.ajax({
                    type:'POST',
                    url:'/sendPaypal',
                    data:{details:details},
                    success:function(data) {
                        
                       
                    }
            })
          }
    </script>



@if($payment)
  <script>
    window.onload = function () {
    if (localStorage.getItem("hasCodeRunBefore") === null) {
        document.getElementById('paypal-button-container').click()

        localStorage.setItem("hasCodeRunBefore", true);
    }
  }
  </script>
@endif

@endsection
@section('css')
<style>
#paypalflash{
      display:flex;
      align-items:center;
      justify-content:center;
      margin-top:100px;
    }
    #paypalflash #flash{
      display:none;
      padding:10px 20px;
      background-color:rgba(16, 3, 83, 0.808);
      border-radius:5px;
    }
    #paypalflash #flash h5{
      font-weight:bold;
      color:white;
    }
</style>

@endsection


    