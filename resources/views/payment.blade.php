@extends('layouts.app')


@section('content')
    <div class="head">
        {{-- <h1>
            TO ADD MORE SAVINGS TO YOUR BCC APP OR YOUR <span style="display:block">CARD</span>
        </h1> --}}
         <p>
            To purchase, or ADD More BCC to your account, simply fill out your details below and enter your code or name. If you do not have a code enter <b>001 to get the BONUS</b>
        </p>
    </div>
    
    <div class="payment_container">
    
        <form action="{{url('/payment')}}" method="post" class="payment-form">
            @csrf
            <div class="form-group">
                <label class="lable-input">Name*</label>
                <input type="text" class="form-control" value="{{old('name')}}" name="name">
            </div>
             @if($errors->has('name'))
                <p class="help-block red">
                    {{ $errors->first('name') }}
                </p>
            @endif
            <div class="form-group">
                <label class="lable-input">Email*</label>
                <input type="text" class="form-control" value="{{old('email')}}" name="email">
            </div>
             @if($errors->has('email'))
                <p class="help-block red">
                    {{ $errors->first('email') }}
                </p>
            @endif
            <div class="form-group">
                <label class="lable-input">phone*</label>
                <input type="text" class="form-control" value="{{old('phone')}}" name="phone">
            </div>
             @if($errors->has('phone'))
                <p class="help-block red">
                    {{ $errors->first('phone') }}
                </p>
            @endif
            <div class="form-group">
                <label class="lable-input">Address:</label>
                <input type="text" class="form-control" value="{{old('address')}}" name="address">
            </div>
             @if($errors->has('address'))
                <p class="help-block red">
                    {{ $errors->first('address') }}
                </p>
            @endif
            <div class="form-group">
                <label class="lable-input">Referral Code*</label>
                <input type="text" class="form-control" value="{{old('code')}}" name="code">
            </div>
             @if($errors->has('code'))
                <p class="help-block red">
                    {{ $errors->first('code') }}
                </p>
            @endif
            <div class="form-group">
                <label class="lable-input">Choose Payment Option</label>
                @if($cost)
                    @if($cost=='30')
                        <div class="form-text">
                            <input type="radio" name="payment" value="30" checked><span><b>:<img src="/images/Imagesss/img/credit.jpg" class="credit-img"><div class="bring-down">$30 BUYS YOU $1,200 PLUS $1,000 OF CASH SAVINGS LOADED ONTO YOUR BCC APP.</div></b></span>
                        </div>
                        {{-- <div class="form-text">
                            <input type="radio" name="payment" value="60"><span><b>:<img src="/images/Imagesss/img/credit.jpg" class="credit-img"><div class="bring-down"> $60 BUYS YOU $4,000 PLUS $1,000 OF CASH SAVINGS LOADED ONTO YOUR BCC APP.</div></b></span>
                        </div> --}}
                    {{-- @elseif($cost=='60')
                        <div class="form-text">
                            <input type="radio" name="payment" value="30" ><span><b>:<img src="/images/Imagesss/img/credit.jpg" class="credit-img"> <div class="bring-down">$30 BUYS YOU $1,200 PLUS $1,000 OF CASH SAVINGS LOADED ONTO YOUR BCC APP.</div></b></span>
                        </div>
                        <div class="form-text">
                            <input type="radio" name="payment" value="60" checked><span><b>:<img src="/images/Imagesss/img/credit.jpg" class="credit-img"><div class="bring-down"> $60 BUYS YOU $4,000 PLUS $1,000 OF CASH SAVINGS LOADED ONTO YOUR BCC APP.</div></b></span>
                        </div> --}}
                    @endif
                @else
                        <div class="form-text">
                            <input type="radio" name="payment" value="30" ><span><b>:<img src="/images/Imagesss/img/credit.jpg" class="credit-img"><div class="bring-down"> $30 BUYS YOU $1,200 PLUS $1,000 OF CASH SAVINGS LOADED ONTO YOUR BCC APP.</div></b></span>
                        </div>
                        {{-- <div class="form-text">
                            <input type="radio" name="payment" value="60" ><span><b>:<img src="/images/Imagesss/img/credit.jpg" class="credit-img"> <div class="bring-down">$60 BUYS YOU $4,000 PLUS $1,000 OF CASH SAVINGS LOADED ONTO YOUR BCC APP.</div></b></span>
                        </div>                     --}}
                @endif
                
            </div>
             @if($errors->has('payment'))
                <p class="help-block red">
                    {{ $errors->first('payment') }}
                </p>
            @endif
            <button type="submit" class="btn-submit2">Submit</button>
        <form>
    </div>

@endsection

@section('css')
    <style>
        .payment_container{
            margin:10px 80px;
        }
        .head{
            display:flex;
            align-items:center;
            justify-content:center;
            text-align:center;
            margin:20px 10%;
        }
        .red{
            color:red;
        }
        .form-text{
            margin-top:20px;
            display:block;
        }
        
        .head p{
            font-size:35px;
            font-family:'Open Sans', sans-serif;
            font-weight:700;
        }
        .lable-input{
            font-weight:bold;
            font-size:1.1rem;
        }
        .credit-img{
            width:40px;
            margin-left:5px;
            height:30px;
        }
        .bring-down{
            display:block;
            margin-top:10px;
            font-size:1.4em;
        }
        @media screen and (max-width:768px){
            .payment_container{
                margin:10px 20px;
            }
            .head p{
                font-size:25px; 
            }
            .lable-input{
                font-size:16px; 
            }
            .form-text span{
                font-size:14px;
            }
            .bring-down{
                font-size:1.1em!important;
            }
            .head{
                margin:20px 5%;
            }
        }
        @media screen and (max-width:450px){
             .head p{
                font-size:20px; 
            }
        }
        .btn-submit{
            padding:8px;
            background-color:rgba(0,32,102,1);
            color:white;
            border-radius:10px;
        }
        .payment-form{
            margin-bottom:200px;
            box-shadow:none;
        }
    </style>
@endsection
{{-- images of bussiness , address , name of the company, phone number --}}
