@component('mail::message')

<h1>Paypal mail</h1>

<b>transition id:</b> {{$paypal->trans_id}}<br>
<b>Status:</b> {{$paypal->status}}<br>
<b>payer id:</b> {{$paypal->payer_id}}<br>
<b>payer first name:</b> {{$paypal->payer_firstname}}<br>
<b>payer surname name :</b> {{$paypal->payer_surname}}<br>
<b>payer email:</b> {{$paypal->payer_email}} <br>
<b>amount:</b> {{$paypal->amount}}<br>
<b>payer address:</b> {{$paypal->payer_address}}<br>
<b>currency code:</b> {{$paypal->currency_code}}<br>


@component('mail::button', ['url' => url("/home")])
Go to Site
@endcomponent

<br>
@endcomponent
