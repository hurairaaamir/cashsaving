@component('mail::message')

<h1>Payment Mail</h1>

<b>name:</b> {{$form->name}}<br>
<b>email:</b> {{$form->email}}<br>
<b>phone:</b> {{$form->phone}}<br>
<b>address:</b> {{$form->address}}<br>
<b>payment:</b> {{$form->payment}} AUS<br>
<b>code:</b> {{$form->code}}<br>



@component('mail::button', ['url' => url("/home")])
Go to Site
@endcomponent

<br>
@endcomponent
