@component('mail::message')

<h1>Contact mail</h1>

<b>name:</b> {{$contact->name}}<br>
<b>subject:</b> {{$contact->subject}}<br>
<b>email:</b> {{$contact->email}}<br>
<b>description:</b> {{$contact->description}}<br>
<b>phone no:</b> {{$contact->phone}}<br>
<b>code:</b> {{$contact->code}}<br>



@component('mail::button', ['url' => url("/home")])
Go to Site
@endcomponent

<br>
@endcomponent
