<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

{{-- paypal --}}
    <meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Ensures optimal rendering on mobile devices. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->
{{-- paypal end --}}

    <title>CashSavings4Consumers</title>
    <link rel="icon" type="image/jpg" href="/images/sales/logo-min.png" style="border-radius:100%;"/>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet" />

    
    <link href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans:ital,wght@1,700&family=Poppins:wght@700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="/css/owl.carousel.min.css" />
    <link rel="stylesheet" href="/css/owl.theme.default.min.css" />
    <link rel="stylesheet" href="/css/main.css" />

    @yield('css')
    
    <script src="{{asset('js/nav.js')}}" defer></script>
    <link rel="stylesheet" href="{{asset('/css/nav.css')}}">



<script
  src="https://code.jquery.com/jquery-3.4.1.js"
  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
  crossorigin="anonymous"></script>
    @yield('js')

    {{-- <script src="js/wow.min.js"></script> --}}
    <!-- This our own file -->

    <script src="/js/owl.carousel.min.js" defer></script>
    <script src="/js/script1.js" defer></script>
</head>
    
<body>
        @include('partials.site_nav')

        @yield('content')

        @include('partials.footer')

</body>


</html>